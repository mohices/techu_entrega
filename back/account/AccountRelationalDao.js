var mysql=require('../db/mysqlcon');

module.exports = {
  insert:function(id,balance, resultCallback){
    mysql.getConnection(function(err, connection) {
      if(err) {
        console.log(err);
        resultCallback(err);
        return;
      }
      var sql = "INSERT INTO Account SET idExAccount=?, balance=?";
      connection.query(sql, [id,balance], function(err, results) {
        console.log(results);
        connection.release(); // always put connection back in pool after last query
        if(err) {
          console.log(err);
          resultCallback(err);
          return;
        }
        resultCallback(null, results);
      });
    });
  },//fin insert
  selectByIdEx:function(idEx,resultCallback){
    mysql.getConnection(function(err, connection) {
      if(err) {
        console.log(err);
        resultCallback(err);
        return;
      }
      var sql = "SELECT * FROM Account WHERE idExAccount=?";
      connection.query(sql, [idEx], function(err, results) {
        console.log(results);
        connection.release(); // always put connection back in pool after last query
        if(err) {
          console.log(err);
          resultCallback(err);
          return;
        }
        if ((results)&&(results.length>0))
          return resultCallback(null, results[0]);
        resultCallback('Data not found');
      });
    });
  }//fin selectByIdEx
};//fin exports
