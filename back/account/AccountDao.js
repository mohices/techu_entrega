var mlab=require('../db/mlab');

module.exports = {
  findById:function(id, resultCallback){
    mlab.findById("account",id, resultCallback);
  },//fin findOne
  findOne:function(json, resultCallback){
    mlab.findOne("account",json, resultCallback);
  },//fin findOne
  findMany:function(json, resultCallback){
    mlab.findMany("account",json, resultCallback);
  },//fin findMany
  insertOne:function(json,resultCallback ){
    mlab.insertOne("account",json, resultCallback);
  },//fin insertOne
  getNextSequence:function(resultCallback ){
    mlab.findAndModify("counters",{ _id: 'accountid' },{ '$inc': { seq: 1 } },
      function(err,body){
        if ((!err)&&(body)&&(body.seq)){
          console.log('Response:'+body.seq);
          resultCallback(null,body.seq);
        }
        else resultCallback(err,null);
      });
  }//fin insertOne
};//fin exports
