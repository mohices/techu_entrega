var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var Account = require('../account/AccountDao');
var AccountRelational = require('../account/AccountRelationalDao');

router.get('/user/:id',VerifyToken, function(req, res) {
  console.log(req.params.id);
  if (req.params.id!=req.userId)
    return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  Account.findMany({ idUser: req.params.id}, function (err, accounts) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if ((accounts) &&(accounts.length>0)){
      var index = 0;
      //console.log("ID:"+user._id);
      accounts.forEach(function(item) {
        console.log("account:item:"+JSON.stringify(item));
        AccountRelational.selectByIdEx(item.Number,function (err,result) {
          if (!err) {
            console.log("AccountRelational result:"+JSON.stringify(result));
            item.relInfo = result;
          }
          index++;
          if(index === accounts.length) {
            console.log("Account result:"+JSON.stringify(accounts));
            res.status(200).send(JSON.stringify(accounts));
           }
        });
      })
    }
    else return res.status(404).send({"error":'No accounts found'});
  });
});//End get accounts by user
router.post('/add',VerifyToken, function(req, res) {
  var dataAccount = {
    "name": req.body.name,
    "date": new Date(),
    "idUser": req.body.idUser,
    "IBAN":"ES91",
    "bankCode":"0004",
    "defaultAccount": req.body.defaultAccount
  }
  console.log(req.body.idUser);
  if (req.body.idUser!=req.userId)
    return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  Account.getNextSequence(function (err,idAccount) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (idAccount){
      console.log(idAccount);
      dataAccount.Number=idAccount;
      Account.insertOne(dataAccount, function (err,account) {
        if (err) return res.status(500).send({"error":'Error on the server.'});
        AccountRelational.insert(idAccount,req.body.balance,function (err,result) {
          if (err) return res.status(500).send({"error":'Error on the server.'});
          res.status(200).send(JSON.stringify(account));
        });
        //if (count!=1) return res.status(500).send('Error on the server.');
        //if (!user) return res.status(404).send('No user found.');
      });
    }
    else return res.status(500).send({"error":'Error on the server'});
  });
});//End add acount
module.exports = router;
