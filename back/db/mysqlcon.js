var config = require('../config');
var mysql = require('mysql');

// var connection = mysql.createConnection(
//     {
//       host     : config.mysqlHost,
//       user     : config.mysqlUser,
//       password : config.mysqlPass,
//       database : config.mysqlDBName
//     }
// );

var pool  = mysql.createPool(
  {
    host     : config.mysqlHost,
    user     : config.mysqlUser,
    password : config.mysqlPass,
    database : config.mysqlDBName
  }
);
module.exports = pool;
