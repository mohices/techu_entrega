var config = require('../config');
var baseMlabUrl=config.mlabUrl;
//TODO: apiKey PUT enviroment variable and insert with docker
var apiKey =config.apiKey;
var requestjson=require('request-json');
module.exports = {
  findById:function(collection,id, resultCallback){
    //console.log(baseMlabUrl);
    //console.log("user/"+id+"?"+apiKey);
    var httpClient = requestjson.createClient(baseMlabUrl);
    httpClient.get(collection +"/"+id+"?"+apiKey,
      function(err,reqMlab,body){
        console.log(err);
        console.log(JSON.stringify(body));
        if ((!err)&&(body)){
          resultCallback(null,body);
        }
        else resultCallback(err,null);
      });
  },//fin findOne
  findOne:function(collection,json, resultCallback){
    var strJson = JSON.stringify(json)
    var query ='q='+ strJson;
    console.log(collection);
    console.log(query);
    var httpClient = requestjson.createClient(baseMlabUrl);
    httpClient.get(collection+"?"+query+'&'+apiKey,
      function(err,reqMlab,body){
        console.log(err);
        console.log(body);
        if ((!err)&&(body.length>0)){
          resultCallback(null,body[0]);
        }
        else resultCallback(err,null);
      });
  },//fin findOne
  findMany:function(collection,json, resultCallback){
    var strJson = JSON.stringify(json)
    var query ='q='+ strJson;
    console.log(query);
    var httpClient = requestjson.createClient(baseMlabUrl);
    httpClient.get(collection+"?"+query+'&'+apiKey,
      function(err,reqMlab,body){
        if ((!err)&&(body.length>0)){
          resultCallback(null,body);
        }
        else resultCallback(err,null);
      });
  },//fin findMany
  findAndModify:function(collection,qJson, bJson, resultCallback){
    var strJson = JSON.stringify(qJson)
    var query ='q='+ strJson;
    console.log(baseMlabUrl);
    console.log(collection+"?"+query+'&'+apiKey);
    console.log(JSON.stringify(bJson));
    var httpClient = requestjson.createClient(baseMlabUrl);
    httpClient.put(collection+"?"+query+'&'+apiKey,bJson,
      function(err,reqMlab,body){
        console.log('ERROR PUT:'+err);
        console.log('BODY PUT:'+JSON.stringify(body));
        if ((!err)&&(body)&&(body.n==1)){
            console.log(JSON.stringify(qJson));
            console.log(collection +"/"+qJson._id+"?"+apiKey);
            httpClient.get(collection +"/"+qJson._id+"?"+apiKey,
            function(err,reqMlab,body){
              console.log('ERROR PUT:'+err);
              console.log('BODY PUT:'+JSON.stringify(body));
              if ((!err)&&(body)){
                console.log('Response:'+body);
                resultCallback(null,body);
              }
              else resultCallback(err,null);
              // if ((!err)&&(body)&&(body.seq)){
              //   console.log('Response:'+body.seq);
              //   resultCallback(null,body.seq);
              // }
              // else resultCallback(err,null);
            });
        }
        else resultCallback(err,null);
      });
  },//fin findAndModify
  insertOne:function(collection,json,resultCallback ){
    console.log(baseMlabUrl);
    console.log("user?"+apiKey);
    console.log(JSON.stringify(json));
    var httpClient = requestjson.createClient(baseMlabUrl);
    httpClient.post(collection+"?"+apiKey, json,
      function(errPut,reqMlab,body){
        console.log('ERROR PUT:'+errPut);
        console.log('BODY PUT:'+JSON.stringify(body));
        if (!errPut){
          resultCallback(null,body);
        }
        else resultCallback(errPut);
    });
  }//fin insertOne
};//fin exports
