var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var config = require('../config');
var VerifyToken = require('../auth/VerifyToken');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var User = require('../user/UserDao');

router.get('/id/:id',VerifyToken, function(req, res) {
  console.log(req.params.id);
  if (req.params.id!=req.userId)
    return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  User.findById(req.params.id, function (err, user) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (user) {
      console.log("ID:"+user._id);
      res.status(200).send(JSON.stringify(user));
    }
    else return res.status(404).send({"error":'user not found'});
  });
});//End get user
router.get('/email/:email',VerifyToken, function(req, res) {
  console.log(req.params.email);
  User.findOne({ email: req.params.email}, function (err, user) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (user) {
      console.log("ID:"+user._id["$oid"]);
      if (user._id["$oid"]!=req.userId)
        return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
      res.status(200).send(JSON.stringify(user));
    }
    else return res.status(404).send({"error":'user not found'});
  });
});//End get user
router.post('/add',/*VerifyToken,*/ function(req, res) {
  var dataUser = {
    "email": req.body.email,
    "password": bcrypt.hashSync(req.body.password, 8),
    "first_name": req.body.first_name,
    "last_name": req.body.last_name/*,
    "id": req.body.id*/
  }
  User.findOne({ email: req.body.email}, function (err, user) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (!user) {
      // If user does not exist yet
      User.insertOne(dataUser, function (err,user) {
        if (err) return res.status(500).send({"error":'Error on the server.'});
        //if (count!=1) return res.status(500).send('Error on the server.');
        //if (!user) return res.status(404).send('No user found.');

        console.log(JSON.stringify(user));
        //res.status(200).send(JSON.stringify(user));
        // if user is found and password is valid
        // create a token
        var token = jwt.sign({ id: user._id }, config.secret, {
          expiresIn: 86400 // expires in 24 hours
        });

        // return the information including toke*/n as JSON
        res.status(200).send({ auth: true, token: token, user: user});
      });
    }
    else return res.status(200).send({"error":'user already exist'});
  });
});//End add user

router.get('/test', function(req, res) {
  res.status(200).send({ auth: false, token: null });
});//End test user

module.exports = router;
