
var mlab=require('../db/mlab');

module.exports = {
  findById:function(id, resultCallback){
    mlab.findById("user",id, resultCallback);
  },//fin findOne
  findOne:function(json, resultCallback){
    mlab.findOne("user",json, resultCallback);
  },//fin findOne
  insertOne:function(json,resultCallback ){
    mlab.insertOne("user",json, resultCallback);
  }//fin insertOne
};//fin exports
