var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var card = require('../card/CardDao');
var account = require('../account/AccountDao');
var cardRelational = require('../card/CardRelationalDao');
var accountRelational = require('../account/AccountRelationalDao');

router.get('/user/:id',VerifyToken, function(req, res) {
  console.log(req.params.id);
  if (req.params.id!=req.userId)
    return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  card.findMany({ idUser: req.params.id}, function (err, cards) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (cards) {
      //console.log("ID:"+user._id);
      res.status(200).send(JSON.stringify(cards));
    }
    else return res.status(404).send({"error":'No cards found'});
  });
});//End get cards by user
router.post('/add', VerifyToken, function(req, res) {
  var dataCard = {
    "name": req.body.name,
    "date": new Date(new Date().getFullYear() + 3, new Date().getMonth()),
    "idUser": req.body.idUser,
    "idAccount": req.body.idAccount,
    "BIN":1234,
    "CVV": 123
  }
  console.log(req.body.idUser);
  if (req.body.idUser!=req.userId)
    return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  card.getNextSequence(function (err,idCard) {
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (idCard){
      console.log(idCard);
      dataCard.Number=idCard;
      card.insertOne(dataCard, function (err,card) {
        if (err) return res.status(500).send({"error":'Error on the server.'});
        console.log('findById:'+dataCard.idAccount);
        account.findById(dataCard.idAccount, function (err, account) {
          console.log(err);
          if (err) return res.status(500).send({"error":'Error on the server.'});
          console.log(JSON.stringify(account));
          accountRelational.selectByIdEx(account.Number,function (err,accountRel) {
            if (err) return res.status(500).send({"error":'Error on the server.'});
            console.log(JSON.stringify(accountRel));
            console.log('Account rel id:'+accountRel.id);
            cardRelational.insert(idCard,600,accountRel.id,function (err,result) {
              if (err) return res.status(500).send({"error":'Error on the server.'});
              res.status(200).send(JSON.stringify(card));
            });
          });
        });
      });
    }
    else return res.status(500).send({"error":'Error on the server'});
  });
});//End add card
module.exports = router;
