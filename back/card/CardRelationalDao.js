var mysql=require('../db/mysqlcon');

module.exports = {
  insert:function(id,limit,idAccount, resultCallback){
    mysql.getConnection(function(err, connection) {
      if(err) {
        console.log(err);
        resultCallback(err);
        return;
      }
      var sql = "INSERT INTO Card SET idExCard=?, monthLimit=?, idAccount=?";
      console.log('idExCard:'+id);
      console.log('limit:'+limit);
      console.log('idAccount:'+idAccount);
      connection.query(sql, [id,limit,idAccount], function(err, results) {
        console.log(results);
        connection.release(); // always put connection back in pool after last query
        if(err) {
          console.log(err);
          resultCallback(err);
          return;
        }
        resultCallback(null, results);
      });
    });
  }//fin insert
};//fin exports
