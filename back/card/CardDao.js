var mlab=require('../db/mlab');

module.exports = {
  findById:function(id, resultCallback){
    mlab.findById("card",id, resultCallback);
  },//fin findOne
  findOne:function(json, resultCallback){
    mlab.findOne("card",json, resultCallback);
  },//fin findOne
  findMany:function(json, resultCallback){
    mlab.findMany("card",json, resultCallback);
  },//fin findMany
  insertOne:function(json,resultCallback ){
    mlab.insertOne("card",json, resultCallback);
  },//fin insertOne
  getNextSequence:function(resultCallback ){
    mlab.findAndModify("counters",{ _id: 'cardid' },{ '$inc': { seq: 1 } },
      function(err,body){
        if ((!err)&&(body)&&(body.seq)){
          console.log('Response:'+body.seq);
          resultCallback(null,body.seq);
        }
        else resultCallback(err,null);
      });
  }//fin insertOne
};//fin exports
