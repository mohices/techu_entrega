var users = require('./Users.json');
var bcrypt = require('bcryptjs');
for (fUser of users) {
  console.log(fUser);
  fUser.password = bcrypt.hashSync(fUser.first_name, 8);
}
writeUserDataToFile(users,'./Users2.json');
function writeUserDataToFile(data,file){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
    file,
    jsonUserData,
    "utf8",
    function(err){
      if (err){
        console.log(err);
      }
      else{
        console.log("Fichero de usuarios persistido");
      }
    }
  )
}
