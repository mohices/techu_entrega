var mysql=require('../db/mysqlcon');
var mlab=require('../db/mlab');

function _insertMove(connection,idSourceAccount,idTargetAccount,idSourceCard,idTargetCard,amount,resultCallback){
  var sql = "INSERT INTO Movement SET idSourceAccount=?,idTargetAccount=?,idSourceCard=?,idTargetCard=?, amount=?";
  var params =[idSourceAccount,idTargetAccount,idSourceCard,idTargetCard,amount];
  //var sql = "INSERT INTO Movement SET idSourceAccount=?,idTargetAccount=?, amount=?";
  //var params =[idSourceAccount,idTargetAccount,amount];
  console.log(sql);
  console.log(JSON.stringify(params));
  connection.query(sql, params, function(err, results) {
    console.log(JSON.stringify(results));
    //console.log(results.insertId);
    console.log(err);
    if ((!err)&&(results))
      return resultCallback(err,results.insertId);
    else return resultCallback(err,results);
  });
}
function _updateBalance(connection,idAccount,amount,resultCallback){
  var sql = "UPDATE Account SET balance = balance + ? WHERE id=?";
  var params=[amount,idAccount];
  console.log(sql);
  console.log(JSON.stringify(params));
  connection.query(sql, params, function(err, results) {
    console.log(JSON.stringify(results));
    console.log(err);
    resultCallback(err,results);
  });
}
function _getBalance(connection,idAccount,resultCallback){
  var sql = "SELECT balance FROM Account WHERE id=?";
  var params=[idAccount];
  console.log(sql);
  console.log(JSON.stringify(params));
  connection.query(sql, params, function(err, results) {
    console.log(JSON.stringify(results));
    console.log(err);
    if (results.length!=1) return resultCallback(err,null);
    if (results[0].balance==undefined) return resultCallback(err,null);
    console.log(JSON.stringify(results[0].balance));
    return resultCallback(err,results[0].balance);
  });
}
function _getMovements(connection,idAccount,resultCallback){
  var sql = "SELECT id,idSourceAccount,idTargetAccount,-amount as amount FROM Movement where idSourceAccount = ? union all SELECT id,idSourceAccount,idTargetAccount,amount as amount FROM Movement where idTargetAccount = ? order by id desc";
  var params=[idAccount,idAccount];
  console.log(sql);
  console.log(JSON.stringify(params));
  connection.query(sql, params, function(err, results) {
    console.log(JSON.stringify(results));
    console.log(err);
    if (results.length<=0) return resultCallback(err,null);
    if (results==undefined) return resultCallback(err,null);
    console.log('OK _getMovements');
    return resultCallback(err,results);
  });
}
function _getIdAccountByNumber(connection,number,resultCallback){
  var sql = "SELECT id FROM Account WHERE idExAccount=?";
  var params=[number];
  console.log(sql);
  console.log(JSON.stringify(params));
  connection.query(sql, params, function(err, results) {
    console.log(JSON.stringify(results));
    console.log(err);
    if (results.length!=1) return resultCallback(err,null);
    if (results[0].id==undefined) return resultCallback(err,null);
    console.log(JSON.stringify(results[0].id));
    return resultCallback(err,results[0].id);
  });
}
function _rollback(connection,resultCallback){
  connection.rollback(function() {
    connection.release();
    resultCallback();
  });
}
function _commit(connection,resultCallback){
  connection.commit(function(errCommit) {
    if (errCommit) {
      _rollback(connection,function(){});
      return resultCallback(errCommit);
    }
    connection.release();
    return resultCallback(null);
  });
}
function _initConnectionWithTransaction(resultCallback){
  mysql.getConnection(function(err, connection) {
    if(err) {
      console.log(err);
      return resultCallback(err);
    }
    connection.beginTransaction(function(err) {
      if(err) {
        console.log(err);
        connection.release();
        return resultCallback(err);
      }
      return resultCallback(null,connection);
    });
  });
}
function _initConnection(resultCallback){
  mysql.getConnection(function(err, connection) {
    if(err) {
      console.log(err);
      return resultCallback(err);
    }
    return resultCallback(null,connection);
  });
}
module.exports = {
  getUserIdByAccountNumber:function(numberAccount,resultCallback){
    mlab.findOne("account",{"Number":parseInt(numberAccount)}, function(err, result){
      if(err) return resultCallback(err,null);
      console.log(result);
      console.log("ID:"+result.idUser);
      if(!result) return resultCallback({"err":"User not found"},null);
      return resultCallback(err,result.idUser);
    });
  },//fin getUserIdByAccountNumber
  checkAccountNumberBalance:function(numberAccount,amount, resultCallback){
    _initConnection(function(err, connection) {
      if(err) return resultCallback(err,false);
      _getIdAccountByNumber(connection,numberAccount,function(err, sourceAccount){
        if(err) return resultCallback(err,false);
        if(sourceAccount==null) return resultCallback(err,false);
        _getBalance(connection,sourceAccount,function(err, balance){
          connection.release();
          if((err)||(balance==null)) {
            return resultCallback(err,false);
          }
          if(amount<=balance){
            return resultCallback(null,true,sourceAccount);
          }
          else{
            return resultCallback(err,false);
          }
        });//Fin _getBalance
      });//Fin _getIdAccountByNumber
    });//Fin _initConnection
  },//fin checkAccountBalance
  checkAccountBalance:function(sourceAccount,amount, resultCallback){
    _initConnection(function(err, connection) {
      if(err) return resultCallback(err,false);
      _getBalance(connection,sourceAccount,function(err, balance){
        connection.release();
        if((err)||(balance==null)) {
          return resultCallback(err,false);
        }
        if(amount<=balance){
          return resultCallback(null,true);
        }
        else{
          return resultCallback(err,false);
        }
      });//Fin _getBalance
    });//Fin _initConnection
  },//fin checkAccountBalance
  getMovemetsByAccountNumber:function(numberAccount, resultCallback){
    _initConnection(function(err, connection) {
      if(err) return resultCallback(err,false);
      _getIdAccountByNumber(connection,numberAccount,function(err, sourceAccount){
        if(err) return resultCallback(err,false);
        if(sourceAccount==null) return resultCallback(err,false);
        _getMovements(connection,sourceAccount,function(err, movements){
          console.log("_getMovements error:" + err);
          connection.release();
          if((err)||(movements==null)) {
            return resultCallback(err,false);
          }
          console.log("getMovemetsByAccountNumber OK:");
          return resultCallback(null,movements);
        });//Fin _getMovements
      });//Fin _getIdAccountByNumber
    });//Fin _initConnection
  },//fin getMovemetsByAccountNumber
  insertAccountNumberMove:function(sourceNumberAccount,targeNumbertAccount,amount, resultCallback){
    _initConnectionWithTransaction(function(err, connection) {
      if(err) return resultCallback(err);
      _getIdAccountByNumber(connection,sourceNumberAccount,function(err,sourceAccount){
        if(err) return resultCallback(err);
        if(sourceAccount==null) return resultCallback({"err":"Source account not found"});
        _getIdAccountByNumber(connection,targeNumbertAccount,function(err,targetAccount){
          if(err) return resultCallback(err);
          _insertMove(connection,sourceAccount,targetAccount,
                null,null,amount,function(err,idInsert){
            if(err) {
              _rollback(connection,function(){});
              return resultCallback(err);
            }
            _updateBalance(connection,sourceAccount,-amount,function(err,numFiles){
              if(err) {
                _rollback(connection,function(){});
                return resultCallback(err);
              }
              _updateBalance(connection,targetAccount,amount,function(err,numFiles){
                if(err) {
                  _rollback(connection,function(){});
                  return resultCallback(err);
                }
                _commit(connection,function(err){
                  if(err) {
                    return resultCallback(err);
                  }
                  return resultCallback(null,idInsert);
                })//Fin commit
              });//Fin _updateBalance target
            });//Fin _updateBalance source
          });//Fin _insertMove
        });//Fin _getIdAccountByNumber target
      });//Fin _getIdAccountByNumber source
    });//Fin _initConnectionWithTransaction
  },//fin insertAccountMove
  insertAccountMove:function(sourceAccount,targetAccount,amount, resultCallback){
    _initConnectionWithTransaction(function(err, connection) {
      if(err) return resultCallback(err);
      _insertMove(connection,sourceAccount,targetAccount,
            null,null,amount,function(err,idInsert){
        if(err) {
          _rollback(connection,function(){});
          return resultCallback(err);
        }
        _updateBalance(connection,sourceAccount,-amount,function(err,numFiles){
          if(err) {
            _rollback(connection,function(){});
            return resultCallback(err);
          }
          _updateBalance(connection,targetAccount,amount,function(err,numFiles){
            if(err) {
              _rollback(connection,function(){});
              return resultCallback(err);
            }
            _commit(connection,function(err){
              if(err) {
                return resultCallback(err);
              }
              return resultCallback(null,idInsert);
            })//Fin commit
          });// _updateBalance target
        });// _updateBalance source
      });//_insertMove
    });//Fin _initConnectionWithTransaction
  }//fin insertAccountMove
};//fin exports
