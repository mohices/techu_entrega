var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var VerifyToken = require('../auth/VerifyToken');
var VerifyOperationToken = require('../auth/VerifyOperationToken');
var mail = require('../mail/mail');
var config = require('../config'); // get config file

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var movement = require('../movement/MovementRelationalDao');

router.post('/add',VerifyToken, function(req, res) {
  //console.log(req.params.id);
  //if (req.params.id!=req.userId)
  //  return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  console.log(JSON.stringify(req.body));
  movement.checkAccountBalance(req.body.idSourceAccount,req.body.amount, function (err, result){
    if (err) return res.status(500).send({"error":'Error on the server.'});
    if (result) {
      movement.insertAccountMove(req.body.idSourceAccount,
          req.body.idTargetAccount,req.body.amount,function (err, result) {
        if (err) return res.status(500).send({"error":'Error on the server.'});
        if (result) {
          //console.log("ID:"+user._id);
          return res.status(200).send(JSON.stringify(result));
        }
        else return res.status(404).send({"error":'Not accounts found'});
      });//fin insertAccountMove
    }
    else return res.status(403).send({"error":'Insufficient funds'});
  });// fin checkAccountBalance
});//End add movement
router.post('/generateOperationToken',VerifyToken, function(req, res) {
  //console.log(req.params.id);
  //if (req.params.id!=req.userId)
  //  return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
  console.log(JSON.stringify(req.body));
  movement.getUserIdByAccountNumber(req.body.sourceNumberAccount,function (err, result){
    if (err) return res.status(500).send({"error":'Error on the server.'});
    console.log(req.userId);
    console.log(result);
    if (result!=req.userId)
      return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
    movement.checkAccountNumberBalance(req.body.sourceNumberAccount,req.body.amount, function (err, result){
      if (err) return res.status(500).send({"error":'Error on the server.'});
      if (result) {
        var random = Math.floor((Math.random() * 10000) + 1);
        var token = jwt.sign({ id: req.userId,account:req.body.sourceNumberAccount,amount:req.body.amount,pin: random }, config.secret, {
          expiresIn: 900 // expires in 15 minutos
        });
        mail.sendMail(req.body.email,req.body.sourceNumberAccount,req.body.amount,random,function (err, result){
          if (err) return res.status(500).send({"error":'Error on the server.'});
          res.status(200).send({ sendMail: true, token: token });
        });
      }
      else return res.status(403).send({"error":'Insufficient funds'});
    });// fin checkAccountBalance
  });//Fin getUserIdByAccountNumber
});//End add movement
router.post('/addByNumber',VerifyToken,VerifyOperationToken, function(req, res) {
  console.log(JSON.stringify(req.body));
  if (req.body.pin!=req.operationTokenData.pin)
    return res.status(403).send({ auth: false, message: 'Invalid operation token provided.' });
  movement.getUserIdByAccountNumber(req.body.sourceNumberAccount,function (err, result){
    if (err) return res.status(500).send({"error":'Error on the server.'});
    console.log(req.userId);
    console.log(result);
    if (result!=req.userId)
      return res.status(403).send({ auth: false, message: 'Invalid token provided.' });
    movement.checkAccountNumberBalance(req.body.sourceNumberAccount,req.body.amount, function (err, result){
      if (err) return res.status(500).send({"error":'Error on the server.'});
      if (result) {
        movement.insertAccountNumberMove(req.body.sourceNumberAccount,
            req.body.targetNumberAccount,req.body.amount,function (err, result) {
          if (err) return res.status(500).send({"error":'Error on the server.'});
          if (result) {
            //console.log("ID:"+user._id);
            return res.status(200).send(JSON.stringify(result));
          }
          else return res.status(404).send({"error":'Not accounts found'});
        });//fin insertAccountMove
      }
      else return res.status(403).send({"error":'Insufficient funds'});
    });// fin checkAccountBalance
  });//Fin getUserIdByAccountNumber
});//End add movement
router.get('/account/:number',VerifyToken, function(req, res) {
  console.log(req.params.number);
  movement.getUserIdByAccountNumber(req.params.number,function (err, result){
    if (err) return res.status(500).send({"error":'Error on the server.'});
    console.log(req.userId);
    console.log(result);
    if (result!=req.userId)
      return res.status(403).send({ auth: false, message: 'Invalid token provided.' });

    movement.getMovemetsByAccountNumber(req.params.number, function (err, result){
      if (err) return res.status(500).send({"error":'Error on the server.'});
      if (result) {
          return res.status(200).send(JSON.stringify(result));
      }
      else return res.status(403).send({"error":'No movements'});
    });// fin checkAccountBalance


  });//Fin getUserIdByAccountNumber
});//End get accounts by user
module.exports = router;
