
var nodemailer = require('nodemailer');
var config = require('../config');

module.exports = {
  sendMail:function(email,account,amount,pin, resultCallback){
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'ebank.apitechu@gmail.com',
        pass: config.mailPassword
      }
    });
    var mailOptions = {
      from: 'ebank.apitechu@gmail.com',
      to: email,
      subject: 'ebank operation pin',
      text: 'The pin for the transfer from ' + account + ', with amount ' + amount +', is:' + pin
    };
    console.log('email1:ebank.apitechu@gmail.com');
    console.log('email2:'+email);
    //console.log('pass:'+config.mailPassword);
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
        console.log(info);
        return resultCallback(error,null);
      } else {
        console.log('Email sent: ' + info.response);
        return resultCallback(null,info.response);
      }
    });
  }//sendMail
};//fin exports
