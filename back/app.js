var fs = require('fs');

var hskey = fs.readFileSync('./keys/TechU-key.pem');
var hscert = fs.readFileSync('./keys/TechU-cert.pem')
var options = {
    key: hskey,
    cert: hscert
};
//var app = require('express').createServer(options);
//var express = require('express');
//var app = express(options);
//var app = express.createServer(options);
//var db = require('./db');
//global.__root   = __dirname + '/';
var express = require('express');
var https = require('https');
var http = require('http');
var cors = require('cors')
var app = express();

var httpServer = http.createServer(app);//.listen(80);
var httpsServer = https.createServer(options, app);//.listen(443);

app.use(cors())
// app.all('/', function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "X-Requested-With");
//   next();
//  });

app.get('/apitechu', function (req, res) {
  res.status(200).send('Apitechu works.');
});

var movementController = require('./movement/MovementController');
app.use('/apitechu/movements', movementController);

var cardController = require('./card/CardController');
app.use('/apitechu/cards', cardController);

var accountController = require('./account/AccountController');
app.use('/apitechu/accounts', accountController);

var userController = require('./user/UserController');
app.use('/apitechu/users', userController);

var authController = require('./auth/AuthController');
app.use('/apitechu/auth', authController);

module.exports = httpsServer;//httpServer;//httpsServer;
